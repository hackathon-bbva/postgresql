FROM postgres:latest

ENV POSTGRES_USER=datastream
ENV POSTGRES_PASSWORD=datastream

ADD database.tar.gz /docker-entrypoint-initdb.d/
